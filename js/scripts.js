$(document).ready(function(){
    $('#mycarousel').carousel({interval:2000});
    $('#carouselButton').click(function(){
        if($('#carouselButton').children('span').hasClass('fa-pause')){
            $('#mycarousel').carousel('pause');
            $('#carouselButton').children('span').removeClass('fa-pause');
            $('#carouselButton').children('span').addClass('fa-play');
        }
        else  if($('#carouselButton').children('span').hasClass('fa-play')){
            $('#mycarousel').carousel('cycle');
            $('#carouselButton').children('span').removeClass('fa-play');
            $('#carouselButton').children('span').addClass('fa-pause');
        }
        
    });

    $('#findridebtn').click(function() {
        $('#offerride').hide();
        $('#findride').show();
        $('#testimonial').hide();
      });

      $('#offerridebtn').click(function() {
        $('#offerride').show();
        $('#findride').hide();
        $('#testimonial').hide();
      });

      $('#testimonialbtn').click(function() {
        $('#offerride').hide();
        $('#findride').hide();
        $('#testimonial').show();
      });

      $('#findridelnk').click(function() {
        $('#offerride').hide();
        $('#findride').show();
        $('#testimonial').hide();
      });

      $('#offerridelnk').click(function() {
        $('#offerride').show();
        $('#findride').hide();
        $('#testimonial').hide();
      });
})

 